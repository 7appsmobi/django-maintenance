import datetime

from django.core import management

from daemons.management import ScheduledCommand


class Command(ScheduledCommand):
    def launch_process(self):
        management.call_command('engineer', maintenance=True)

    @classmethod
    def next_call(cls):
        next_call = datetime.datetime.now()
        next_call = datetime.datetime(next_call.year,
                                      next_call.month,
                                      next_call.day,
                                      4,
                                      0,
                                      0)
        next_call -= datetime.timedelta(days=next_call.weekday())
        if next_call < datetime.datetime.now():
            next_call += datetime.timedelta(days=7)
        return next_call
