import os
from django.conf import settings

from maintenance.logs import log_file

if settings.DEBUG:
    FILEMANAGER_STATIC_ROOT = getattr(settings, 'FILEMANAGER_STATIC_ROOT',
                                      os.path.dirname(os.path.abspath(__file__)) + '/static/filemanager/')
else:
    FILEMANAGER_STATIC_ROOT = getattr(settings, 'FILEMANAGER_STATIC_ROOT',
                                      settings.STATIC_ROOT + 'filemanager/')

FILEMANAGER_CKEDITOR_JS = getattr(settings, 'FILEMANAGER_CKEDITOR_JS',
                                  'ckeditor/ckeditor.js')

FILEMANAGER_CHECK_SPACE = getattr(settings, 'FILEMANAGER_CHECK_SPACE',
                                  False)

FILEMANAGER_SHOW_SPACE = getattr(settings, 'FILEMANAGER_SHOW_SPACE',
                                 FILEMANAGER_CHECK_SPACE)

LOGTAIL_UPDATE_INTERVAL = getattr(settings, 'LOGTAIL_UPDATE_INTERVAL', '3000')

LOGTAIL_FILES = getattr(settings, 'LOGTAIL_FILES', {
    'django': log_file(settings.LOGS_ROOT),
    'apache-custom': os.path.join(settings.LOGS_ROOT, 'apache-custom.log'),
    'apache-error': os.path.join(settings.LOGS_ROOT, 'apache-error.log'),
})
