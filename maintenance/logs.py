import os


def logging_filter(record):
    message = record.getMessage()
    if 'RemovedInDjango19Warning' in message:
        return False
    return True


def log_file(log_root):
    return os.path.join(log_root, 'django.log')


def init_logging(log_root):
    log_filepath = log_file(log_root)
    if not os.path.exists(log_filepath):
        open(log_filepath, 'a').close()
        os.chmod(log_filepath, 0777)

    return {
        'version': 1,
        'disable_existing_loggers': True,
        'filters': {
            'require_debug_false': {
                '()': 'django.utils.log.RequireDebugFalse',
            },
            'require_debug_true': {
                '()': 'django.utils.log.RequireDebugTrue',
            },
            'skip_deprecated': {
                '()': 'django.utils.log.CallbackFilter',
                'callback': logging_filter,
            }
        },
        'formatters': {
            'verbose': {
                'format': "[%(asctime)s] %(levelname)s [%(name)s:%(lineno)s] %(message)s",
                'datefmt': "%d/%b/%Y %H:%M:%S"
            },
            'simple': {
                'format': '%(levelname)s %(message)s'
            },
        },
        'handlers': {
            'file': {
                'class': 'logging.handlers.RotatingFileHandler',
                'filename': log_filepath,
                'maxBytes': 1024 * 1024 * 10,
                'backupCount': 3,
                'formatter': 'verbose',
                'filters': ['skip_deprecated', 'require_debug_false', ]
            },
            'console': {
                'class': 'logging.StreamHandler',
                'filters': ['skip_deprecated', 'require_debug_true', ]
            }
        },
        'loggers': {
            '': {
                'handlers': ['file', 'console', ],
                'propagate': True,
                'level': 'WARNING',
            },
        }
    }