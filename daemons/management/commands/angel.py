import pprint
from optparse import make_option
import os
import re
from django.conf import settings
from django.core.management import BaseCommand
import time

from daemons.daemon_class import Daemon
from maintenance.utils import log, die


class Command(BaseCommand):
    option_list = BaseCommand.option_list + (
        make_option(
            '--copy',
            action='store',
        ),
        make_option(
            '--daemon_mode',
            action='store',
        ),
        make_option(
            '--copies',
            action='store',
            default=1,
        ),
    )

    modes = None
    managepy = ''

    @staticmethod
    def managepy_path():
        return os.path.join(settings.BASE_DIR, 'manage.py')

    @staticmethod
    def run_django_command(command):
        os_command = "%s %s" % (Command.managepy_path(), command)
        log("running %s" % os_command)
        os.system(os_command)

    def daemon_command(self, command, daemon_name, **options):
        suffix = '--private'

        if options.get('copy'):
            suffix += ' --copy=%d' % int(options['copy'])

        if options.get('daemon_mode'):
            suffix += ' --daemon_mode=%s' % options['daemon_mode']

        if options.get('force'):
            suffix += ' --force'

        daemon_command = "daemon %s %s %s" % (command, daemon_name, suffix)
        Command.run_django_command(daemon_command)

    def is_running(self, pidname):
        return Daemon(pidfile=os.path.join(settings.RUNTIME_ROOT, pidname)).is_running()

    def daemons_running(self, **options):
        files = os.listdir(settings.RUNTIME_ROOT)

        result = {}
        for filename in files:
            if not self.is_running(filename):
                continue

            def add_daemon(name, copy=None, mode=None):
                if name not in result:
                    result[name] = []
                if copy:
                    copy = int(copy)
                result[name].append({'copy': copy, 'mode': mode})

            pidfile_match_moded = re.match(r'^([a-z0-9\-_]+)__c(\d+)__m([a-z0-9\-_]+)\.pid$', filename)
            if pidfile_match_moded:
                data = pidfile_match_moded.groups()
                add_daemon(data[0], data[1], data[2])
                continue

            pidfile_match_copy = re.match(r'([a-z0-9\-_]+)__c(\d+)\.pid$', filename)
            if pidfile_match_copy:
                data = pidfile_match_copy.groups()
                add_daemon(data[0], data[1])
                continue

            pidfile_match = re.match(r'^([a-z0-9\-_]+)\.pid$', filename)
            if pidfile_match:
                data = pidfile_match.groups()
                add_daemon(data[0])

        return result

    def show_daemons_running(self, *args, **kwargs):
        pprint.pprint(self.daemons_running())

    def daemon(self, **options):
        log("A am the server master. Behold.")

        while True:
            daemons_running = self.daemons_running()

            log("checking needed daemons working")
            for daemon_command, mode_config in self.modes.items():
                max_copies = mode_config.get('copies', 1)
                modes = mode_config.get('modes')

                def check_daemon(daemon_copy=None, daemon_mode=None):
                    if daemon_mode is not None:
                        daemon_mode = str(daemon_mode)

                    pidname = Daemon.pidname(daemon_command, daemon_copy, daemon_mode)
                    if not self.is_running(pidname):
                        log('daemon %s is not running, restarting again...' % pidname)
                        kwargs = {'copy': daemon_copy}
                        if daemon_mode:
                            kwargs['daemon_mode'] = daemon_mode
                        self.daemon_command('restart', daemon_command, **kwargs)
                    # else:
                        # log('daemon %s ok' % pidname)

                    if daemon_command not in daemons_running:
                        return

                    daemons_temp = daemons_running[daemon_command]
                    daemons_temp = [d for d in daemons_temp if d['copy'] != daemon_copy or d['mode'] != daemon_mode]
                    daemons_running[daemon_command] = daemons_temp

                for copy in range(1, max_copies + 1):
                    if modes:
                        for mode in modes:
                            check_daemon(copy, mode)
                    elif mode_config.get('copies'):
                        check_daemon(copy)
                    else:
                        check_daemon()

            log('daemons mode after filtering %s' % pprint.pformat(daemons_running))

            for daemon_command, daemons in daemons_running.items():
                if daemon_command not in self.modes:
                    log("%s daemon working but unmanaged" % daemon_command)
                    continue

                for daemon in daemons:
                    log("going to stop daemon %s copy=%s mode=%s" %
                        (daemon_command, str(daemon['copy']), daemon['mode']))

                    self.daemon_command('stop',
                                        daemon_command,
                                        daemon_mode=daemon['mode'],
                                        copy=daemon['copy'],
                                        force=True)

            time.sleep(30)

    def send_all(self, command):
        def get_mode_sorter(sorter_mode):
            if sorter_mode == 'angel' and command == 'stop':
                return -100
            return self.modes[sorter_mode].get('sorter', 0)

        for mode in sorted(self.modes.keys(), key=get_mode_sorter):
            self.daemon_command(command, mode)

        if settings.PRODUCTION and command == 'restart':
            os.system('/etc/init.d/apache2 reload')

    def start_all(self, **options):
        self.send_all('start')

    def start(self, mode, **options):
        self.daemon_command('start', mode, **options)

    def restart_all(self, **options):
        self.send_all('restart')

    def restart(self, mode, **options):
        self.daemon_command('restart', mode, **options)

    def stop_all(self, **options):
        self.send_all('stop')

    def stop(self, mode, **options):
        self.daemon_command('stop', mode, **options)

    def handle(self, mode='daemon', *args, **options):
        if not hasattr(settings, 'ANGEL_CONFIG'):
            die('Please set ANGEL_CONFIG in settings')

        self.modes = settings.ANGEL_CONFIG
        # log("modes in %s" % self.modes)

        self.managepy = os.path.join(settings.BASE_DIR, 'manage.py')
        if options.get('max', None):
            settings.MAX_WORKER_PROCESSES = 100

        log("%s %s %s" % (mode, args, options))
        result = getattr(self, mode)(*args, **options)
        if result:
            log(pprint.pformat("%s"))
