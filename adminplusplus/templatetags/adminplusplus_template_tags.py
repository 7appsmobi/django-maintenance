# coding=utf-8
from django.contrib.admin import site
from django.apps import apps
from django.utils.module_loading import import_string
from django.utils.text import capfirst
from django.core.urlresolvers import reverse, NoReverseMatch
from django.core.exceptions import ImproperlyConfigured
from django.utils import six
from django.conf import settings
from django import template
from django import VERSION as DJANGO_VERSION

# Depending on you python version, reduce has been moved to functools
try:
    from functools import reduce
except ImportError:
    pass

register = template.Library()

MAX_LENGTH_BOOTSTRAP_COLUMN = 12


def css_classes_for_field(field, custom_classes):
    orig_class = field.field.widget.attrs.get('class', '')
    required = 'required' if field.field.required else ''
    classes = field.css_classes(' '.join([orig_class, custom_classes, required]))
    return classes


@register.simple_tag(takes_context=True)
def header_additional_controls(context):
    additional_controls = getattr(settings, "HEADER_ADDITIONAL_COTROLS", None)
    if additional_controls:
        return import_string(additional_controls)(context=context)
    else:
        return ''

@register.simple_tag
def host():
    return getattr(settings, 'HOST', getattr(settings, 'ALLOWED_HOSTS', ['admin'])[0])


@register.filter()
def get_label(field, custom_classes=''):
    classes = css_classes_for_field(field, custom_classes)
    return field.label_tag(attrs={'class': classes}, label_suffix='')


@register.filter()
def add_class(field, custom_classes=''):
    classes = css_classes_for_field(field, custom_classes)
    try:
        # For widgets like SelectMultiple, checkboxselectmultiple
        field.field.widget.widget.attrs.update({'class': classes})
    except:
        field.field.widget.attrs.update({'class': classes})
    return field


@register.filter()
def widget_type(field):
    if isinstance(field, dict):
        return 'adminreadonlyfield'
    try:
        # For widgets like SelectMultiple, checkboxselectmultiple
        widget_type = field.field.widget.widget.__class__.__name__.lower()
    except:
        widget_type = field.field.widget.__class__.__name__.lower()
    return widget_type


@register.filter()
def placeholder(field, placeholder=''):
    field.field.widget.attrs.update({'placeholder': placeholder})
    return field


def sidebar_menu_setting():
    return getattr(settings, 'LEFT_MENU', False)


@register.assignment_tag
def display_sidebar_menu(has_filters=False):
    if has_filters:
        # Always display the menu in change_list.html
        return True
    return sidebar_menu_setting()


#
# Используется для правильного вывода по категориям
# на главной странице админки
@register.filter
def bigregroup(custom_dict, app_list):
    for app in app_list:
        app_group = app.get('app_group')
        if custom_dict.get(app_group):
            if app not in custom_dict[app_group]:
                custom_dict[app_group].append(app)
        else:
            custom_dict[app_group] = [app, ]

    custom_list = []
    if hasattr(settings, 'LEFT_MENU'):
        for index, item in enumerate(settings.LEFT_MENU):
            if custom_dict.get(item):
                custom_list.insert(index, {item: custom_dict.pop(item)})

    if len(custom_dict):
        for item in custom_dict.keys():
            custom_list.append({item: custom_dict.get(item)})

    return custom_list


@register.inclusion_tag('adminplusplus/left_menu.html',
                        takes_context=True)
def render_adminpluslpus_menu_app_list(context):
    show_global_menu = sidebar_menu_setting()
    if not show_global_menu:
        return {'items_list': ''}

    if DJANGO_VERSION < (1, 8):
        dependencie = 'django.core.context_processors.request'
        processors = settings.TEMPLATE_CONTEXT_PROCESSORS
        dependency_str = 'settings.TEMPLATE_CONTEXT_PROCESSORS'
    else:
        dependencie = 'django.template.context_processors.request'
        implemented_engines = getattr(settings, 'BOOTSTRAP_ADMIN_ENGINES',
            ['django.template.backends.django.DjangoTemplates'])
        dependency_str = "the 'context_processors' 'OPTION' of one of the " + \
            "following engines: %s" % implemented_engines
        filtered_engines = [engine for engine in settings.TEMPLATES
            if engine['BACKEND'] in implemented_engines]
        if len(filtered_engines) == 0:
            raise ImproperlyConfigured(
                "bootstrap_admin: No compatible template engine found" +
                "bootstrap_admin requires one of the following engines: %s"
                % implemented_engines
            )
        processors = reduce(lambda x, y: x.extend(y), [
            engine.get('OPTIONS', {}).get('context_processors', [])
            for engine in filtered_engines])

    if dependencie not in processors:
        raise ImproperlyConfigured(
            "bootstrap_admin: in order to use the 'sidebar menu' requires" +
            " the '%s' to be added to %s"
            % (dependencie, dependency_str)
        )


    #

    custom_dict = {}
    for path, view, name, urlname, visible, group in site.custom_views:
        if visible is True:
            if group:
                if custom_dict.get(group):
                    if name:
                        custom_dict[group].append((path, name))
                    else:
                        custom_dict[group].append((path, capfirst(view.__name__)))
                else:
                    if name:
                        custom_dict[group] = [(path, name), ]
                    else:
                        custom_dict[group] = [(path, capfirst(view.__name__)), ]

    # Code adapted from django.contrib.admin.AdminSite
    app_dict = {}
    user = context.get('user')
    for model, model_admin in site._registry.items():
        app_label = model._meta.app_label
        app_group = site._app_groups.get(app_label,'OTHER')
        has_module_perms = user.has_module_perms(app_label)

        if has_module_perms:
            perms = model_admin.get_model_perms(context.get('request'))

            # Check whether user has any perm for this module.
            # If so, add the module to the model_list.
            if True in perms.values():
                info = (app_label, model._meta.model_name)
                model_dict = {
                    'name': capfirst(model._meta.verbose_name_plural),
                    'object_name': model._meta.object_name,
                    'perms': perms,
                }
                if perms.get('change', False):
                    try:
                        model_dict['admin_url'] = reverse(
                            'admin:%s_%s_changelist' % info,
                            current_app=site.name
                        )
                    except NoReverseMatch:
                        pass
                if perms.get('add', False):
                    try:
                        model_dict['add_url'] = reverse(
                            'admin:%s_%s_add' % info,
                            current_app=site.name
                        )
                    except NoReverseMatch:
                        pass
                if app_label in app_dict:
                    app_dict[app_label]['models'].append(model_dict)
                else:
                    app_dict[app_label] = {
                        'name': apps.get_app_config(app_label).verbose_name,
                        'app_label': app_label,
                        'app_group': app_group,
                        'app_url': reverse(
                            'admin:app_list',
                            kwargs={'app_label': app_label},
                            current_app=site.name
                        ),
                        'has_module_perms': has_module_perms,
                        'models': [model_dict],
                    }

    # Sort the apps alphabetically.
    app_list = list(six.itervalues(app_dict))
    app_list.sort(key=lambda x: x['name'].lower())

    # Sort the models alphabetically within each sapp.
    for app in app_list:
        app['models'].sort(key=lambda x: x['name'])

    return {'app_list': app_list, 'custom_dict': custom_dict, 'current_url': context.get('request').path}


@register.filter()
def class_for_field_boxes(line):
    size_column = MAX_LENGTH_BOOTSTRAP_COLUMN / len(line.fields)
    return 'col-sm-{0}'.format(size_column or 1)  # if '0' replace with 1
