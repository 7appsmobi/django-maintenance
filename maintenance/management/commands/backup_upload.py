from django.core.management import BaseCommand
from django.conf import settings
import os

from maintenance.utils import die, log

import_error = None
try:
    from googleapiclient.http import MediaFileUpload
    import httplib2
    from apiclient import discovery
    import oauth2client
    from oauth2client import client
    from oauth2client import tools
except Exception as e:
    import_error = e

SCOPES = [
    'https://www.googleapis.com/auth/drive'
]

CURRENT_DIR = os.path.dirname(os.path.realpath(__file__))
MAINTENANCE_DIR = os.path.dirname(os.path.dirname(CURRENT_DIR))

CLIENT_SECRET_FILE = os.path.join(MAINTENANCE_DIR, "data/client_secret.json")

APPLICATION_NAME = 'Backups'

if hasattr(settings, 'BACKUPS_GOOGLE_DISK_PARENT_FOLDER'):
    BACKUPS_GOOGLE_DISK_PARENT_FOLDER = settings.BACKUPS_GOOGLE_DISK_PARENT_FOLDER
else:
    BACKUPS_GOOGLE_DISK_PARENT_FOLDER = 'Django'

if hasattr(settings, 'BACKUPS_GOOGLE_DISK_FOLDER'):
    BACKUPS_GOOGLE_DISK_FOLDER = settings.BACKUPS_GOOGLE_DISK_FOLDER
else:
    BACKUPS_GOOGLE_DISK_FOLDER = settings.ALLOWED_HOSTS[0]


class CredentialsFlags(object):
    logging_level = "DEBUG"
    noauth_local_webserver = True


def get_credentials():
    """Gets valid user credentials from storage.

    If nothing has been stored, or if the stored credentials are invalid,
    the OAuth2 flow is completed to obtain the new credentials.

    Returns:
        Credentials, the obtained credential.
    """
    credential_dir = settings.DATA_ROOT
    if not os.path.exists(credential_dir):
        os.makedirs(credential_dir)
    credential_path = os.path.join(credential_dir, 'drive-quickstart.json')

    store = oauth2client.file.Storage(credential_path)
    credentials = store.get()
    if not credentials or credentials.invalid:
        flow = client.flow_from_clientsecrets(CLIENT_SECRET_FILE, SCOPES)
        flow.user_agent = APPLICATION_NAME

        credentials = tools.run_flow(flow, store, CredentialsFlags())
        print 'Storing credentials to ' + credential_path
    return credentials


class Command(BaseCommand):
    google_service = None

    def make_folder(self, title, parent=None):
        folder_insert_params = {
            "title": title,
            "mimeType": "application/vnd.google-apps.folder",
        }
        if parent:
            folder_insert_params["parents"] = [{"id": parent}]
        results = self.google_service.files().insert(body=folder_insert_params).execute()
        return results["id"]

    def find_file(self, title, parent_id=None):
        server_query = "title = '%s'" % title
        if parent_id:
            server_query += "and '%s' in parents" % parent_id
        result = self.google_service.files().list(q=server_query).execute()
        return result.get('items', [])

    def handle(self, *args, **options):
        if import_error:
            raise Exception('Import error: %s' % import_error)

        if not hasattr(settings, 'DATA_ROOT'):
            raise Exception('Please set DATA_ROOT in settings')

        if not hasattr(settings, 'BACKUP_ROOT'):
            raise Exception('Please set BACKUP_ROOT in settings')

        credentials = get_credentials()
        http = credentials.authorize(httplib2.Http())
        self.google_service = discovery.build('drive', 'v2', http=http)

        items = self.find_file(BACKUPS_GOOGLE_DISK_PARENT_FOLDER, 'root')
        if len(items):
            root_folder_id = items[0]["id"]
        else:
            root_folder_id = self.make_folder(BACKUPS_GOOGLE_DISK_PARENT_FOLDER)

        items = self.find_file(BACKUPS_GOOGLE_DISK_FOLDER, root_folder_id)
        if len(items):
            parent_folder_id = items[0]["id"]
        else:
            parent_folder_id = self.make_folder(BACKUPS_GOOGLE_DISK_FOLDER, parent=root_folder_id)

        log("Parent folder is %s (%s / %s)" % (
            parent_folder_id, BACKUPS_GOOGLE_DISK_PARENT_FOLDER, BACKUPS_GOOGLE_DISK_FOLDER
        ))

        file_list = os.listdir(settings.BACKUP_ROOT)
        file_list = [f for f in file_list if os.path.isfile(os.path.join(settings.BACKUP_ROOT, f))]

        files = self.google_service.files()

        for filename in file_list:
            items = self.find_file(filename, parent_folder_id)
            if len(items):
                log("skipping %s %s" % (filename, items[0]['alternateLink']))
                continue
            else:
                log("uploading %s")
            absolute_path = os.path.join(settings.BACKUP_ROOT, filename)

            uploader = MediaFileUpload(absolute_path, resumable=True)
            request = files.insert(media_body=uploader, body={
                'parents': [{'id': parent_folder_id, }],
                'title': filename,
                'mimeType': 'application/zip',
            })
            response = None
            while response is None:
                status, response = request.next_chunk()
                if status:
                    log("Uploaded %d%%." % int(status.progress() * 100))

            log("Upload Complete!!")
