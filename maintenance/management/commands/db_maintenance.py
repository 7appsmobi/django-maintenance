# coding=utf-8
import datetime
import pprint
from subprocess import call
import os

import subprocess

import shutil
from django.conf import settings
from django.core.management import BaseCommand
from django.db import ProgrammingError
from django.db import connections, OperationalError

from maintenance.utils import die, log


class Command(BaseCommand):
    help = "usage: engineer command [mode]"
    force = False
    engine = 'pgsql'
    database = 'default'

    def add_arguments(self, parser):
        parser.add_argument('command',
                            nargs='?',
                            help='engineer command (backup_db/check_db/reset_db/create_db/restore_backup)')
        parser.add_argument('--force',
                            help='force command',
                            default=False,
                            action='store_true',
                            dest='force')
        parser.add_argument('--backup', help='restore backup', action='store', dest='backup')
        parser.add_argument('--database', help='database', action='store', dest='database', default='default')
        parser.add_argument('-t', '--table', help='tables to backup', action='store', dest='table')

    def user_interact(self, question=None):
        if self.force:
            return True

        if question:
            print question
        answer = raw_input('yes/no? ')
        return answer == 'yes'

    def db_password(self):
        return settings.DATABASES[self.database]['PASSWORD']

    def db_name(self):
        # pprint.pprint(settings.DATABASES)
        return settings.DATABASES[self.database]['NAME']

    def db_user(self):
        return settings.DATABASES[self.database]['USER']

    def drop_db(self, *args, **options):
        db_name = self.db_name()
        log('dropping database %s' % db_name)
        cursor = self.master_connection().cursor()

        try:
            cursor.execute('drop database %s' % db_name)
        except ProgrammingError as e:
            if 'database "%s" does not exist' % db_name in e.message:
                log('already not exists')
                return
        except OperationalError as e:
            if 'is being accessed by other users' not in e.message:
                die("OperationalError dropping database %s" % e)

            if self.engine == 'mysql':
                die('cannot mysql drop connections')
        else:
            log("dropping database %s successful" % db_name)
            return

        if not self.user_interact("Drop connections?"):
            return

        sql = "select pg_terminate_backend(pid) from pg_stat_activity where datname='%s'" % db_name
        cursor.execute(sql)
        self.drop_db()

    def create_db(self, *args, **options):
        db_name = self.db_name()
        log('creating database %s' % db_name)
        cursor = self.master_connection().cursor()
        sql = 'create database %s' % db_name
        if self.engine == 'mysql':
            sql += ' default charset=utf8'
        cursor.execute(sql)

    def reset_db(self, *args, **options):
        log("Reseting db erases all data")
        if self.user_interact("Are you sure?"):
            self.drop_db()
            self.create_db()
            return True
        else:
            return False

    def check_db(self, *args, **options):
        try:
            print 'checking database settings'
            pprint.pprint(settings.DATABASES[self.database])
            connections[self.database].cursor()
        except Exception, ex:
            if self.user_interact("Error connecting %s.\nTry to create database?" % ex):
                self.create_db()
        else:
            log('db ok')

    def restore_backup(self, *args, **options):
        backup_file = options.get('backup')
        if not backup_file:
            raise Exception('need to set backup file with --backup=')

        backup_file = os.path.abspath(backup_file)
        if not os.path.isfile(backup_file):
            raise Exception('not found file %s' % backup_file)

        fname, ext = os.path.splitext(backup_file)
        unzip = False

        if ext == '.zip':
            if os.path.splitext(fname)[1] != '.sql':
                raise Exception('not .sql.zip file %s' % backup_file)
            unzip = True
        elif ext != '.sql':
            raise Exception('not .sql file %s' % backup_file)

        reseted = self.reset_db()
        if not reseted:
            return

        os.chdir('/tmp')

        if self.engine == 'mysql':
            password = ""
            if self.db_password():
                password = " -p%s" % self.db_password()
            if unzip:
                command = 'gunzip < %s | mysql -u %s%s %s' % (backup_file, self.db_user(), password, self.db_name())
            else:
                command = 'mysql -u %s%s %s < %s' % (self.db_user(), password, self.db_name(), backup_file)
        elif self.engine == 'pgsql':
            if unzip:
                command = 'gunzip < %s | sudo -u %s psql %s' % (backup_file, self.db_user(), self.db_name())
            else:
                command = 'sudo -u %s psql %s < %s' % (self.db_user(), self.db_name(), backup_file)
        else:
            raise Exception('unknown db driver %s' % self.engine)

        log('calling %s' % command)
        call(command, shell=True)

    def master_connection(self):
        if 'master' not in settings.DATABASES:
            die("Please set master database in settings.py")
        if settings.DATABASES['master']['ENGINE'] != settings.DATABASES[self.database]['ENGINE']:
            die('Cannot use master db from another engine')
        return connections['master']

    def backup_db(self, **options):
        path = options.get('backup')

        if path is None:
            base_dir = settings.BACKUP_ROOT
            if not os.path.exists(base_dir):
                os.mkdir(base_dir)
                os.chmod(base_dir, 0777)
            path = os.path.join(base_dir, "%s.sql.zip" % datetime.datetime.now().strftime("%Y-%m-%d--%H-%M-%S"))

        if self.engine == 'mysql':
            password = ""
            if self.db_password():
                password = " -p%s" % self.db_password()
            dumper = 'mysqldump -u %s%s %s | gzip -9 > %s' % \
                     (self.db_user(), password, self.db_name(), path)
        else:
            suffix = " "
            if options['table']:
                suffix = " -t %s" % options['table']
            dumper = 'sudo -u %s pg_dump %s%s | gzip -9 > %s' % (self.db_user(), self.db_name(), suffix, path)

        """Создание бекапа"""
        log(dumper)
        log('Start dumping into %s' % path)
        os.chdir('/tmp')
        subprocess.call(dumper, shell=True)

        if os.path.getsize(path) == 0:
            os.remove(path)
            die("Empty sql dump. Error?")

    def handle(self, *args, **options):
        command = options.get('command')

        self.force = options.get('force')
        self.database = options.get('database', 'default')

        if 'default' not in settings.DATABASES:
            die("Please set default database in settings.py")

        database_engine = settings.DATABASES[self.database]['ENGINE']

        if database_engine == 'django.db.backends.mysql':
            self.engine = 'mysql'
        elif database_engine != 'django.db.backends.postgresql_psycopg2':
            die("Only postgres and mysql databases is supported")

        backup_file = options.get('backup')
        if backup_file and not command:
            command = 'restore_backup'

        if not command:
            log("usage: manage.py db_maintenance [command]")
            exit()

        getattr(self, command)(**options)
