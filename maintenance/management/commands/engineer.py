# coding=utf-8
from subprocess import call
import os

import shutil
from django.conf import settings
from django.core import management
from django.core.management import BaseCommand

from maintenance.utils import die, log


class Command(BaseCommand):
    help = "usage: engineer command [mode]"

    def add_arguments(self, parser):
        parser.add_argument('command', nargs='?', help='engineer command')
        parser.add_argument('-i', '--install',
                            action='store_true',
                            dest="install",
                            help='calling pip install -r requirements.txt')
        parser.add_argument('-c', '--check',
                            action='store_true',
                            dest="check",
                            help='check required folders')
        parser.add_argument('-u', '--update',
                            action='store_true',
                            dest="update",
                            help='update project')
        parser.add_argument('--skip_angel',
                            action='store_true',
                            help='skip angel operations')
        parser.add_argument('--skip_apache',
                            action='store_true',
                            help='skip apache operations')
        parser.add_argument('--clearlogs',
                            action='store_true',
                            help='clear project logs')
        parser.add_argument('-m', '--maintenance',
                            action='store_true',
                            help='perform project maintenance')

    def get_install_folder(self):
        if hasattr(settings, 'INSTALL_ROOT'):
            return settings.INSTALL_ROOT
        elif not hasattr(settings, 'PROJECT_ROOT'):
            die('Please set PROJECT_ROOT in settings.py')

        install_dir = os.path.join(settings.PROJECT_ROOT, 'install')
        if not os.path.exists(install_dir):
            die('Please set INSTALL_ROOT in settings.py to use engineer functions')

        return install_dir

    def check_folders(self, *args, **options):
        folders_to_check = []
        keys_to_check = ["STATIC_ROOT", "MEDIA_ROOT", "RUNTIME_ROOT",
                         "BACKUP_ROOT", "CACHE_ROOT", "TEMP_FOLDER_ROOT",
                         "DATA_ROOT", ]

        for key in keys_to_check:
            if not hasattr(settings, key):
                continue
            folders_to_check.append(getattr(settings, key))

        if hasattr(settings, 'INSTALL_DIRS'):
            folders_to_check += settings.INSTALL_DIRS

        for folder_path in folders_to_check:
            if not os.path.exists(folder_path):
                log('creating %s' % folder_path)
                os.mkdir(folder_path, 0777)
            os.chmod(folder_path, 0777)

    def install(self, *args, **options):
        if not hasattr(settings, 'PROJECT_ROOT'):
            die('Please set PROJECT_ROOT in settings.py')

        pip_path = os.path.join(settings.PROJECT_ROOT, 'env/bin/pip')
        requirements_path = os.path.join(self.get_install_folder(), 'requirements.txt')
        if not requirements_path:
            die('Not found requirements in %s' % requirements_path)

        print "Going to call pip check modules from %s" % requirements_path
        call([pip_path, 'install', '-r', requirements_path])

    def update(self, *args, **kwargs):
        skip_angel = kwargs.get('skip_angel') or not hasattr(settings, 'ANGEL_CONFIG')

        if not skip_angel:
            management.call_command('angel', 'stop_all')

        self.install(*args, **kwargs)
        os.chdir(settings.PROJECT_ROOT)
        call(['git', 'pull'])
        management.call_command('collectstatic', interactive=False)
        management.call_command('migrate')

        if not skip_angel:
            management.call_command('angel', 'start_all')

        if not kwargs.get('skip_apache'):
            log("going to restart apache2\nservice apache2 reload")
            call(['service', 'apache2', 'reload'])

    def clear_logs(self, skip_angel=False, **kwargs):
        if not hasattr(settings, 'LOGS_ROOT'):
            log("please set LOGS_ROOT in settings")
            return

        if not os.path.exists(settings.LOGS_ROOT):
            log("cannot found %s" % settings.LOGS_ROOT)
            return

        skip_angel = skip_angel or not hasattr(settings, 'ANGEL_CONFIG')

        if not skip_angel:
            management.call_command('angel', 'stop_all')

        log("clearing %s" % settings.LOGS_ROOT)
        shutil.rmtree(settings.LOGS_ROOT)
        os.mkdir(settings.LOGS_ROOT)
        os.chmod(settings.LOGS_ROOT, 0777)

        if not skip_angel:
            management.call_command('angel', 'start_all')

    def maintenance(self, **kwargs):
        management.call_command('angel', 'stop_all')
        try:
            self.clear_logs(skip_angel=True)
        except:
            pass
        management.call_command('backup')
        management.call_command('angel', 'start_all')
        management.call_command('backup_upload')

    def handle(self, *args, **options):
        command = options.get('command')

        if options.get('install'):
            command = 'install'
        elif options.get('check'):
            command = 'check_folders'
        elif options.get('update'):
            command = 'update'
        elif options.get('clearlogs'):
            command = 'clear_logs'
        elif options.get('maintenance'):
            command = 'maintenance'

        if not command:
            print "usage: manage.py engineer [command]"
            exit()

        getattr(self, command)(**options)
