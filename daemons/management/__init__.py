from django.core.management import BaseCommand


class ScheduledCommand(BaseCommand):
    recall_process = False

    def add_arguments(self, parser):
        parser.add_argument('-f', '--force', action='store_true', help='force')

    @classmethod
    def next_call(cls):
        raise Exception('please override')

    def handle(self, *args, **options):
        if options["force"]:
            self.launch_process()
            return

        raise Exception("please call command with --force or through schduler")

    def launch_process(self):
        self.run_process()
        while self.recall_process:
            self.run_process()

    def run_process(self):
        raise Exception('please override')
