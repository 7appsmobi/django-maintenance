from __future__ import absolute_import, unicode_literals

from django.apps import AppConfig

class MaintenanceConfig(AppConfig):
    name = 'maintenance'
    verbose_name = "Service modules and some django utilites"

    # def ready(self):
    #     if 'debug_toolbar' in settings.INSTALLED_APPS:
    #         from debug_toolbar import settings as dt_settings
    #         dt_settings.patch_root_urlconf()
