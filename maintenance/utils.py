import logging
import datetime

from django.conf import settings


def log(message):
    print "[%s] %s" % (datetime.datetime.now(), message)


def error(message):
    logging.getLogger(__name__).error(message)
    log("ERROR: %s" % message)


def die(message):
    error(message)
    exit()


def project_name():
    return os.path.basename(settings.BASE_DIR)