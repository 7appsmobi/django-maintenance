=======================================
Модуль для обслуживания django проектов
=======================================

Требования
~~~~~~~~~~
В проекте необходимо хранить список зависимостей в файле *install/requirements.txt*

Для работы системы обслуживания БД необходимо в *settings.py* добавить базу данных **master** для административных операций

Для работы системы бэкапов в *settings.py* необходимо задать абсолютный адрес папки с бэкапами **BACKUP_ROOT**

Для работы аплоада на google disk в *settings.py* необходимо задать абсолютный адрес папки с данными **DATA_ROOT** и
установить модуль *google-api-python-client*


Установка
~~~~~~~~~
Для установки модуля django-maintenance в зависимости необходимо добавить
``git+ssh://git@bitbucket.org/7appsmobi/django-maintenance``

В *settings.py* добавить в **INSTALLED_APPS** модуль maintenance для использования django management команд

Можно установить django-maintenance глобально для системы, тогда будет доступен скрипт *install_all* для развёртывания поектов


Развёртывание проекта
~~~~~~~~~~~~~~~~~~~~~

``# git clone https://bitbucket.org/7appsmobi/some-django-project``

``# cd some-django-project``

``# install_all``


Скрипт создаст виртуальное окружение, папки необходимые для работы, проверит базу данных


Создание бэкапа
---------------
В *settings.py* могут быть заданы:

**BACKUP_STATIC_DIRS** - массив относительных имён папок которые также будут попадать в бэкап. Имена отноcительно **STATIC_ROOT**

**BACKUP_FOLDER_MAX_SIZE** - максимальный размер папки с бэкапами в мегабайтах. При привышении размера старые бэкапы будут удаляться


``# insanity/manage.py backup``

Бэкап будет создан в папке backups и заархивирован в zip


Восстановление бэкапа
---------------------
Предположим что необходимо восстановить файл backups/backup.zip

``# insanity/manage.py backup -b backups/backup.zip``


Если нужно восстановить последний бэкап в **BACKUP_ROOT**

``# insanity/manage.py backup restore``


Восстановление sql бэкапа
-------------------------
Предположим что необходимо восстановить файл backups/dump.sql

``# insanity/manage.py db_maintenance --backup=dump.sql``


Очистка БД
----------
``# insanity/manage.py db_maintenance reset_db``


Загрузка бэкапов на Google Disk
-------------------------------
В *settings.py* могут быть заданы:

**BACKUPS_GOOGLE_DISK_PARENT_FOLDER** - папка для бэкапов, по умолчанию Django

**BACKUPS_GOOGLE_DISK_FOLDER** - папка внутри предыдущей для конкретного проекта, по умолчанию первый хост из **ALLOWED_HOSTS**

Для использования google disk, в список зависимостей проекта необходимо добавить модуль *google-api-python-client*

``# insanity/manage.py backup_upload``


Проверка структуры папок
------------------------
В *settings.py* могут быть заданы *STATIC_ROOT, MEDIA_ROOT, RUNTIME_ROOT, BACKUP_ROOT,*
*CACHE_ROOT, TEMP_FOLDER_ROOT, DATA_ROOT*

Существование этих папок проверится автоматически. Этот список можно расширить задав массив **INSTALL_DIRS**

Команда проверит существование заданных папок, при необходимости создаст

``# insanity/manage.py engineer -с``


Установка модулей
-----------------
Аналог команды *pip -r install/requirements.txt*

``# insanity/manage.py engineer -i``


Обновление проекта
------------------
Обновление модулей, файлов из git и перезагрузка демонов и сервера

``# insanity/manage.py engineer -u``

