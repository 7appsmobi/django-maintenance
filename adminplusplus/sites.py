# coding=utf-8
import pprint
from functools import wraps
from django.contrib import admin
from django.contrib.admin.sites import AdminSite
from django.contrib.admin import ModelAdmin
from django.db.models.base import ModelBase
from django.http import Http404
from django.apps import apps
from django.conf import settings
from django.core.exceptions import ImproperlyConfigured
from django.core.urlresolvers import NoReverseMatch, reverse
from django.views.decorators.cache import never_cache
from django.utils import six
from django.utils.text import capfirst
from django.template.response import TemplateResponse

class AlreadyRegistered(Exception):
    pass

system_check_errors = []

def superuser_check(f):
     @wraps(f)
     def wrapper(request, *args, **kwds):
        if request.user.is_superuser:
            return f(request, *args, **kwds)
        else:
            raise Http404("YOU SHALL NOT PASS")
     return wrapper


class AdminPlusPlusMixin(object):
    index_template = 'adminplusplus/index.html'

    def __init__(self, *args, **kwargs):
        self._registry = {}
        self.custom_views = []
        super(AdminPlusPlusMixin, self).__init__(*args, **kwargs)

    def register_view(self, path, name=None, urlname=None, visible=True,
                      view=None, group=u'OTHER'):
        if view is not None:
            view = superuser_check(view)
            self.custom_views.append((path, view, name, urlname, visible, group))
        else:
            raise Exception("please set view to register")

        # def decorator(fn):
        #     self.custom_views.append((path, fn, name, urlname, visible, group))
        #     return fn
        # return decorator

    @property
    def urls(self):
        return self.get_urls(), 'admin', self.name

    def get_urls(self):
        """Add our custom views to the admin urlconf."""
        urls = super(AdminPlusPlusMixin, self).get_urls()
        from django.conf.urls import patterns, url
        for path, view, name, urlname, visible, group in self.custom_views:
            urls = patterns(
                '',
                url(r'^%s$' % path, self.admin_view(view), name=urlname),
            ) + urls
        return urls

    def index(self, request, extra_context=None):
        """Make sure our list of custom views is on the index page."""
        if not extra_context:
            extra_context = {}

        custom_dict = {}
        for path, view, name, urlname, visible, group in self.custom_views:
            if visible is True:
                if group:
                    if custom_dict.get(group):
                        if name:
                            custom_dict[group].append((path, name))
                        else:
                            custom_dict[group].append((path, capfirst(view.__name__)))
                    else:
                        if name:
                            custom_dict[group] = [(path, name), ]
                        else:
                            custom_dict[group] = [(path, capfirst(view.__name__)), ]

        extra_context.update({
            'custom_dict': custom_dict
        })
        # print 'from this'
        return super(AdminPlusPlusMixin, self).index(request, extra_context)


# рег моделей с группами
# базовые урлы
# урлы к моделям
class MyAdminSite(AdminSite):
    def __init__(self, name='admin'):
        self._groups = {}
        self._app_groups = {}
        super(MyAdminSite, self).__init__()

    def register(self, model_or_iterable, admin_class=None, group=u'OTHER', **options):
        if not admin_class:
            admin_class = ModelAdmin

        if isinstance(model_or_iterable, ModelBase):
            model_or_iterable = [model_or_iterable]
        for model in model_or_iterable:
            if model._meta.abstract:
                raise ImproperlyConfigured('The model %s is abstract, so it '
                      'cannot be registered with admin.' % model.__name__)

            if model in self._registry:
                raise AlreadyRegistered('The model %s is already registered' % model.__name__)

            if not model._meta.swapped:
                if options:
                    options['__module__'] = __name__
                    admin_class = type("%sAdmin" % model.__name__, (admin_class,), options)

                if admin_class is not ModelAdmin and settings.DEBUG:
                    system_check_errors.extend(admin_class.check(model))

                # Instantiate the admin class to save in the registry
                self._registry[model] = admin_class(model, self)
                self._groups[model] = [group, self._registry[model]] ###


    def set_app_group(self, app_label, group):
        if self._app_groups.get(app_label):
            raise AlreadyRegistered
        self._app_groups[app_label] = group

    @never_cache
    def index(self, request, extra_context=None):
        app_dict = {}
        for model, model_admin in self._registry.items():
            app_label = model._meta.app_label
            app_group = self._app_groups.get(app_label,u'OTHER')
            has_module_perms = model_admin.has_module_permission(request)

            if has_module_perms:
                perms = model_admin.get_model_perms(request)

                if True in perms.values():
                    info = (app_label, model._meta.model_name)
                    model_dict = {
                        'name': capfirst(model._meta.verbose_name_plural),
                        'object_name': model._meta.object_name,
                        'perms': perms,
                    }
                    if perms.get('change', False):
                        try:
                            model_dict['admin_url'] = reverse('admin:%s_%s_changelist' % info, current_app=self.name)
                        except NoReverseMatch:
                            pass
                    if perms.get('add', False):
                        try:
                            model_dict['add_url'] = reverse('admin:%s_%s_add' % info, current_app=self.name)
                        except NoReverseMatch:
                            pass
                    if app_label in app_dict:
                        app_dict[app_label]['models'].append(model_dict)
                    else:
                        app_dict[app_label] = {
                            'name': apps.get_app_config(app_label).verbose_name,
                            'app_label': app_label,
                            'app_group': app_group,
                            'app_url': reverse(
                                'admin:app_list',
                                kwargs={'app_label': app_label},
                                current_app=self.name,
                            ),
                            'has_module_perms': has_module_perms,
                            'models': [model_dict],
                        }

        # Sort the apps alphabetically.
        app_list = list(six.itervalues(app_dict))
        app_list.sort(key=lambda x: x['name'].lower())
        # Sort the models alphabetically within each app.
        for app in app_list:
            app['models'].sort(key=lambda x: x['name'])

        context = dict(
            self.each_context(request),
            title=self.index_title,
            app_list=app_list,
        )
        context.update(extra_context or {})

        request.current_app = self.name
        return TemplateResponse(request, self.index_template or
                                'admin/index.html', context)

# This global object represents the default admin site, for the common case.
# You can instantiate AdminSite in your own code to create a custom admin site.
admin.site = MyAdminSite()


class AdminSitePlusPlus(AdminPlusPlusMixin, MyAdminSite):
    # print 'yellow sword'
    """A Django AdminSite with the AdminPlusMixin to allow registering custom
    views not connected to models."""
