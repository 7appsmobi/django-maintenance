# coding=utf-8

import traceback
import os
import subprocess
import time
import shutil
import datetime

from dircache import listdir

from django.core import management
from django.core.management.base import BaseCommand
from django.conf import settings

from maintenance.utils import die, log


class Command(BaseCommand):
    database = 'default'
    force = False

    def folder_max_size(self):
        if hasattr(settings, 'BACKUP_FOLDER_MAX_SIZE'):
            return settings.BACKUP_FOLDER_MAX_SIZE
        else:
            return 1024 * 5 # MB

    def add_arguments(self, parser):
        parser.add_argument('command', nargs='?', action='store')
        parser.add_argument('-b', '--backup', action='store', dest='backup')
        parser.add_argument('-t', '--tables', action='store', dest='tables')
        parser.add_argument('-p', '--paths', action='store', dest='paths')
        parser.add_argument('--database', help='database', action='store', dest='database', default='default')
        parser.add_argument('--force', help='force command', default=False, action='store_true', dest='force')

    def dumpdata(self, **options):
        from django.apps import apps

        MAX_TABLE_LENGTH = 100000

        if not options.get('tables'):
            die('please call with -t=app.model,app.model2')

        tables = str(options['tables']).split(',')

        now_str = datetime.datetime.now().strftime("%Y-%m-%d--%H-%M-%S")
        basefolder_path = os.path.join(settings.BACKUP_ROOT, now_str)

        for table in tables:
            if '.' not in table:
                log("cannot parse %s" % table)

            (app, model) = table.split('.')

            tablefolder_path = os.path.join(basefolder_path, table)
            os.makedirs(tablefolder_path)

            table_object = apps.get_model(app, model)
            table_length = table_object.objects.all().count()

            if table_length > MAX_TABLE_LENGTH:
                table_ids = table_object.objects.order_by('id').values_list('id', flat=True)
                table_ids = [str(x) for x in table_ids]

                start = 0
                i = 0
                while start < table_length:
                    finish = start + MAX_TABLE_LENGTH
                    joined_ids = ",".join(table_ids[start:finish])
                    if len(joined_ids) > 130000:  # fix for bash. (MAX_ARG_STRLEN = 131072).
                        fix_joined_ids = joined_ids[:130001]
                        finish = start + len(fix_joined_ids.split(',')[:-1])
                        joined_ids = ",".join(table_ids[start:finish])

                    filename = "{0}.json.gz".format(i)
                    path = os.path.join(tablefolder_path, filename)
                    managepy = os.path.join(os.path.join(settings.PROJECT_ROOT, 'insanity'), 'manage.py')
                    os_command = "%s dumpdata %s --pks %s | gzip -9 > %s" % (managepy, table, joined_ids, path)

                    true_finish = finish
                    if finish > table_length:
                        true_finish = table_length-1
                    os_command_str = "%s dumpdata %s --pks %s...%s | gzip -9 > %s" % (managepy,
                                                                                      table,
                                                                                      table_ids[start],
                                                                                      table_ids[true_finish],
                                                                                      path)
                    log("running %s" % os_command_str)
                    os.system(os_command)

                    start = finish
                    i += 1
            else:
                filename = "0.json.gz"
                path = os.path.join(tablefolder_path, filename)
                managepy = os.path.join(os.path.join(settings.PROJECT_ROOT, 'insanity'), 'manage.py')
                os_command = "%s dumpdata %s | gzip -9 > %s" % (managepy, table, path)
                log("running %s" % os_command)
                os.system(os_command)

        os_command_zip = "cd {0} && zip -r {1}.zip {2}".format(settings.BACKUP_ROOT,
                                                               os.path.join(settings.BACKUP_ROOT, now_str),
                                                               now_str)
        log("running %s" % os_command_zip)
        os.system(os_command_zip)
        os_command_rm = "rm -r {0}".format(os.path.join(settings.BACKUP_ROOT, now_str))
        log("running %s" % os_command_rm)
        os.system(os_command_rm)

    def loaddata(self, **options):
        def apply_fixtures(folder_path):
            if os.path.exists(folder_path):
                for fixture in listdir(folder_path):
                    fixture_path = os.path.join(folder_path, fixture)
                    managepy = os.path.join(os.path.join(settings.PROJECT_ROOT, 'insanity'), 'manage.py')
                    os_command = "%s loaddata %s" % (managepy, fixture_path)
                    log("running %s" % os_command)
                    os.system(os_command)
            else:
                log("path do not exist %s " % path)

        if not options.get('paths'):
            die('please call with -p=path/to/app.model/, path/to/app.model2/ where app.model and app.model2 are folders which contains fixtures. path must be in BACKUP_ROOT')

        paths = str(options['paths']).split(',')
        for path in paths:
            if path.split('.')[-1] == 'zip':
                full_path = os.path.abspath(path)
                path = path[:len(path)-4]
                os_command_unzip = "unzip {0} -d {1}".format(full_path, os.path.split(path)[0])
                log("running %s" % os_command_unzip)
                os.system(os_command_unzip)

            path = os.path.abspath(path)
            path_to_folder, folder_name = os.path.split(path)
            if not folder_name:
                folder_name = os.path.split(path_to_folder)[-1]

            if '--' in folder_name:  # ~ 2016-02-19--20-48-52
                for app_model in listdir(path):
                    sub_path = os.path.join(path, app_model)
                    apply_fixtures(sub_path)
            else:  # ~ not App.ModelName
                apply_fixtures(path)

            os_command_rm = "rm -r {0}".format(os.path.join(path))
            log("running %s" % os_command_rm)
            os.system(os_command_rm)

    def handle(self, **options):
        backup_file = options.get('backup')
        command = options.get('command')

        self.database = options.get('database', 'default')
        self.force = options.get('force', False)

        if not command:
            if backup_file:
                command = 'restore'
            else:
                command = 'backup'

        if not hasattr(settings, 'BACKUP_ROOT'):
            die('Please set BACKUP_ROOT in settings')

        if not backup_file and command == 'restore':
            files = os.listdir(settings.BACKUP_ROOT)
            if len(files) > 0:
                max_filename = max(os.listdir(settings.BACKUP_ROOT))
                log('using last log %s' % max_filename)
                backup_file = os.path.join(settings.BACKUP_ROOT, max_filename)
            else:
                log('please select file with --backup')
                return

        options['backup'] = backup_file
        getattr(self, command)(**options)

    def restore(self, **options):
        temp_dir = self.temp_dir()

        backup_file = options.get('backup')
        backup_file = os.path.abspath(backup_file)

        backup_file_dir, backup_file_name = os.path.split(backup_file)
        backup_file_name, backup_file_ext = os.path.splitext(backup_file_name)
        if not backup_file_ext == '.zip':
            log('please select .zip file')
            return

        command = 'unzip %s -d %s' % (backup_file, temp_dir)
        log("%s" % command)
        subprocess.call(command, shell=True)

        working_dir = os.path.join(temp_dir, backup_file_name)
        sql_path = os.path.join(working_dir, '%s.sql.zip' % backup_file_name)

        try:
            self.restore_from(working_dir, sql_path)
        except Exception, ex:
            log('restore from backup failed: %s' % ex)
            log("%s" % traceback.format_exc())
        else:
            log('Restore %s sucsessful!' % backup_file)
        finally:
            log('Deleted folder %s' % temp_dir)
            shutil.rmtree(temp_dir)

    def restore_from(self, temp_dir, sql_path):
        management.call_command('db_maintenance',
                                command='restore_backup',
                                database=self.database,
                                force=self.force,
                                backup=sql_path)

        for static_dir in self.backup_static_dirs():
            cp_source = os.path.join(temp_dir, "%s.zip" % static_dir)
            cp_destination = os.path.join(settings.STATIC_ROOT, static_dir)

            if not os.path.exists(cp_source):
                log("%s not exists, skipping" % cp_source)
                continue

            if os.path.exists(cp_destination):
                log('removing %s' % cp_destination)
                shutil.rmtree(cp_destination)

            command = 'unzip %s -d %s' % (cp_source, settings.STATIC_ROOT)
            log(command)
            subprocess.call(command, shell=True)

    def temp_dir(self, name=None):
        if not name:
            name = str(time.strftime("%Y-%m-%d--%H-%M-%S"))
        dir_path = os.path.join(settings.BACKUP_ROOT, name)
        os.mkdir(dir_path)
        os.chmod(dir_path, 0777)
        return dir_path

    def backup_static_dirs(self):
        if hasattr(settings, 'BACKUP_STATIC_DIRS'):
            return [f.rstrip('/') for f in settings.BACKUP_STATIC_DIRS]
        else:
            return []

    """Проверка на переполнение максимального размера папки"""
    def total_backup_folder_size(self):
        total_size = 0
        for dirpath, dirnames, filenames in os.walk(settings.BACKUP_ROOT):
            log('File list in\t\t%s:' % dirpath)
            for f in sorted(filenames):
                if not f.startswith('.'):
                    fp = os.path.join(dirpath, f)
                    file_size = os.path.getsize(fp) / 1024.0 / 1024.0
                    total_size += file_size
                    log("----\t%s\t%f" % (fp, file_size))
        log("Total size \t\t%s - %.1f MB; Max size\t%sMB" % (settings.BACKUP_ROOT, total_size, self.folder_max_size()))
        return total_size

    def backup(self, **options):
        base_dir = settings.BACKUP_ROOT
        if not os.path.exists(base_dir):
            os.mkdir(base_dir)
            os.chmod(base_dir, 0777)

        time_now = str(time.strftime("%Y-%m-%d--%H-%M-%S"))
        dump_dir = self.temp_dir(time_now)

        sql_dump_path = os.path.join(dump_dir, "%s.sql.zip" % time_now)
        management.call_command('db_maintenance',
                                command='backup_db',
                                database=self.database,
                                backup=sql_dump_path)

        os.chdir(settings.STATIC_ROOT)

        for static_dir in self.backup_static_dirs():
            cp_destination = os.path.join(dump_dir, "%s.zip" % static_dir)

            command = 'zip -r -9 %s ./%s' % (cp_destination, static_dir)
            log(command)
            subprocess.call(command, shell=True)

        os.chdir(settings.BACKUP_ROOT)
        zipper = 'zip -r -0 %s.zip ./%s' % (time_now, time_now)
        log(zipper)
        subprocess.call(zipper, shell=True)

        log('Deleted folder %s' % dump_dir)
        shutil.rmtree(dump_dir)

        try:
            management.call_command('backup_upload')
        except Exception, ex:
            log("exception calling backup_upload %s" % ex)

        total_size = self.total_backup_folder_size()

        while total_size > self.folder_max_size():
            files = os.listdir(base_dir)
            files.sort(key=lambda x: os.path.getmtime(os.path.join(base_dir, x)))
            min_filename = files[0]
            old_file = os.path.join(base_dir, min_filename)
            os.remove(old_file)
            log("Deleted %s" % old_file)
            total_size = self.total_backup_folder_size()
