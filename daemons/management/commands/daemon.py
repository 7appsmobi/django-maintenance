from optparse import make_option
import os
from django.conf import settings
from django.core import management
from django.core.management import BaseCommand, CommandError
from daemons.daemon_class import Daemon
from maintenance.utils import die


class DaemonManager(Daemon):
    def run(self, **kwargs):
        if self.django_management_command:
            management.call_command(self.django_management_command, **kwargs)
        else:
            raise Exception("please enter django_management_command to run daemon")


class Command(BaseCommand):
    help = "usage: daemon command [mode]\nmodes: start, stop, restart, modes, running"
    option_list = BaseCommand.option_list + (
        make_option(
            '--copies',
            action='store',
            default=1,
        ),
        make_option(
            '--copy',
            action='store',
        ),
        make_option(
            '--private',
            action='store_true',
        ),
        make_option(
            '--force',
            action='store_true',
        ),
        make_option(
            '--daemon_mode',
            action='store',
        ),
    )

    def last_log_file(self, mode):
        directory = os.path.join(settings.LOGS_ROOT, mode)
        if not os.path.isdir(directory):
            print "unknown log directory %s" % directory
            return None

        files = [os.path.join(directory, l) for l in os.listdir(directory)]
        if files:
            return max(files, key=os.path.getmtime)
        else:
            return None

    def handle(self, *args, **options):
        if not hasattr(settings, 'LOGS_ROOT'):
            die('Please set LOGS_ROOT in settings')

        if not hasattr(settings, 'RUNTIME_ROOT'):
            die('Please set RUNTIME_ROOT in settings')

        if len(args) < 1:
            raise CommandError('usage: daemon command [mode]')

        command = args[0]
        daemon_name = ''
        if len(args) > 1:
            daemon_name = args[1]

        copy = options.get('copy')
        daemon_mode = options.get('daemon_mode')

        if options['force']:
            if copy:
                copy = int(copy)
            daemon_name_full = Daemon.daemon_name(daemon_name, copy, daemon_mode, force=1)

            print 'going to forced launch %s' % daemon_name_full
            manager = DaemonManager(daemon_name_full,
                                    django_management_command=daemon_name)
            getattr(manager, command)()

            return

        if copy is None:
            copy = 1
        else:
            copy = int(copy)

        if command in ['start', 'stop', 'restart', ]:
            if not options['private'] and daemon_name in settings.ANGEL_CONFIG:
                print 'going to launch through angel'
                management.call_command('angel', command, daemon_name)
                return

            daemon_name_full = Daemon.daemon_name(daemon_name, copy, daemon_mode)

            def simple_launch():
                print 'going to actual launch %s' % daemon_name_full
                manager = DaemonManager(daemon_name_full,
                                        django_management_command=daemon_name)
                getattr(manager, command)()

            if options['force']:
                simple_launch()
                return

            copies = options['copies']
            if copies is None:
                copies = 1
            else:
                copies = int(copies)

            daemon_modes = None
            if daemon_name in settings.ANGEL_CONFIG:
                daemon_config = settings.ANGEL_CONFIG[daemon_name]

                daemon_modes = daemon_config.get('modes')
                copies = daemon_config.get('copies', copies)
            else:
                print "%s not found in %s" % (daemon_name, settings.ANGEL_CONFIG.keys())

            if copies > 1 and not options['copy']:
                print 'going to launch all copies through angel'
                for i in range(1, copies + 1):
                    management.call_command('angel', command, daemon_name, copy=i)
                return

            if daemon_modes:
                if daemon_mode:
                    print "going to actual launch %s for daemon_mode %s" % (daemon_name_full, daemon_mode)
                    manager = DaemonManager(daemon_name_full, django_management_command=daemon_name)
                    getattr(manager, command)(daemon_mode=daemon_mode)
                else:
                    print 'going to launch all modes through angel'
                    for daemon_mode in daemon_modes:
                        management.call_command('angel', command, daemon_name, copy=copy, daemon_mode=daemon_mode)
            else:
                simple_launch()
        elif command == 'log':
            last_log = self.last_log_file(daemon_name)
            if last_log:
                lines = '-n 40'
                if len(args) > 2 and args[2] == 'full':
                    lines = '-n 1000'
                os.system('tail %s -f %s' % (lines, last_log))
        elif command == 'last_log':
            print "%s" % self.last_log_file(daemon_name)
        else:
            print "unknown command"
