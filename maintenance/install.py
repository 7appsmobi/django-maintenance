#!/usr/bin/python
# -*- coding: utf-8 -*-
import pprint
import shutil
from subprocess import call
import sys
import os

#
# Настройки
#

# Можно отменять/разрешать любой этап инсталяции
SKIP_VIRTUAL_ENV = False
SKIP_PIP = False
SKIP_MANAGEMENT_COMMANDS = False


#
# Настройки хостинга
#

POSTGRES_HOST = 'localhost'
POSTGRES_ROOT_USER = 'postgres'
POSTGRES_ROOT_PASSWORD = '777'

if __name__ not in ['maintenance.install', '__main__', ]:
    print "Please call as maintenance.install or __main__ not %s" % __name__
    exit()

PROJECT_ROOT = os.getcwd()
INSTALLER_ROOT = os.path.join(PROJECT_ROOT, 'install')

if not os.path.exists(INSTALLER_ROOT):
    print("Wrong working directory? Cannot find %s" % INSTALLER_ROOT)
    exit()

PROJECT_NAME = None

for filename in os.listdir(PROJECT_ROOT):
    filepath = os.path.join(PROJECT_ROOT, filename)
    if os.path.isdir(filepath):
        if os.path.exists(os.path.join(filepath, 'manage.py')):
            print "found %s" % filename
            PROJECT_NAME = filename
            break

if PROJECT_NAME is None:
    print "cannot auto find project name in %s" % PROJECT_ROOT
    exit()

#
# ИНСТАЛЯТОР
#

# INSTALLER_ROOT = os.path.dirname(os.path.abspath(__file__))
# PROJECT_ROOT = os.path.dirname(INSTALLER_ROOT)

PY_PROJECT_ROOT = os.path.join(PROJECT_ROOT, PROJECT_NAME)

if not os.path.exists(PY_PROJECT_ROOT):
    print("Wrong project name? Cannot find %s" % PY_PROJECT_ROOT)
    exit()
else:
    print "project root is %s" % PROJECT_ROOT

ENV_PATH = os.path.join(PROJECT_ROOT, 'env')
if os.path.exists(ENV_PATH):
    print 'found virtual env at %s. overwrite?' % ENV_PATH
    answer = raw_input('yes/no? ')
    if answer != 'yes':
        SKIP_VIRTUAL_ENV = True
    else:
        shutil.rmtree(ENV_PATH)

if not SKIP_VIRTUAL_ENV:
    print 'installing virtualenv %s' % ENV_PATH
    call(['virtualenv', ENV_PATH, '-p', 'python2.7'])
else:
    print 'skipping virtual env'

ENV_BIN_PATH = os.path.join(ENV_PATH, 'bin')

if not SKIP_PIP:
    print 'updating modules'
    call([os.path.join(ENV_BIN_PATH, 'pip'), 'install', '-r', os.path.join(INSTALLER_ROOT, 'requirements.txt')])

log_dir = os.path.join(PROJECT_ROOT, 'logs')
if not os.path.exists(log_dir):
    print 'creating log dir %s' % log_dir
    os.mkdir(log_dir, 0777)
os.chmod(log_dir, 0777)

os.chdir(PY_PROJECT_ROOT)
sys.path.append(PY_PROJECT_ROOT)
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "%s.settings" % PROJECT_NAME)

ACTIVATE_THIS_PATH = os.path.join(ENV_BIN_PATH, "activate_this.py")
print "going to activate %s" % ACTIVATE_THIS_PATH
execfile(ACTIVATE_THIS_PATH, dict(__file__=ACTIVATE_THIS_PATH))

from django.conf import settings
from django.core import management

if not hasattr(settings, 'INSTALLED_APPS'):
    print "INSTALLED_APPS not in settings. wrong settings module?"
    exit()

if 'maintenance' not in settings.INSTALLED_APPS:
    settings.INSTALLED_APPS += ('maintenance', )

if 'master' not in settings.DATABASES:
    settings.DATABASES['master'] = {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'postgres',
        'USER': POSTGRES_ROOT_USER,
        'PASSWORD': POSTGRES_ROOT_PASSWORD,
        'HOST': POSTGRES_HOST
    }

import django
django.setup()

if hasattr(settings, "LOG_FILE"):
    print "check log file %s" % settings.LOG_FILE
    open(settings.LOG_FILE, 'a').close()
    os.chmod(settings.LOG_FILE, 0777)

if not SKIP_MANAGEMENT_COMMANDS:
    print "Calling check folders"
    management.call_command('engineer', check=True)

    print "Calling collect static"
    management.call_command('collectstatic', interactive=False)

    print "Calling check db"
    management.call_command('db_maintenance', 'check_db')

    print "Calling migrate"
    management.call_command('migrate')
