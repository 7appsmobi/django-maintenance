# coding=utf-8

import pprint
from django.conf import settings
from os.path import isfile, getsize
import json
import os
from django.contrib import admin
from django.conf.urls import url
from django.http import HttpResponse, Http404
from filemanager import settings as app_settings
from django.template.response import TemplateResponse
from filemanager import FileManager, FileManagerForm
from functools import wraps


class HttpUnauthorized(HttpResponse):
    status_code = 401


class ImbeddedFileManager(FileManager):

    def render(self, request, path):

        if path:
            return self.media(path)

        ckeditorfuncnum = request.GET.get('CKEditorFuncNum', '')
        messages = []

        self.current_path = '/'
        self.current_id = 1
        if request.method == 'POST':
            form = FileManagerForm(request.POST, request.FILES)
            if form.is_valid():
                messages = self.handle_form(form, request.FILES)

        if app_settings.FILEMANAGER_CHECK_SPACE:
            space_consumed = self.get_size(self.basepath)
        else:
            space_consumed = 0
        return {
            'dir_structure': self.directory_structure(),
            'messages': map(str, messages),
            'current_id': self.current_id,
            'CKEditorFuncNum': ckeditorfuncnum,
            'ckeditor_baseurl': self.ckeditor_baseurl,
            'public_url_base': self.public_url_base,
            'space_consumed': space_consumed,
            # 'basepath': self.basepath,
            'max_space': self.maxspace,
            'show_space': app_settings.FILEMANAGER_SHOW_SPACE,
            'log_path': settings.LOGS_ROOT
        }




# TODO вот как было бы правильно сделать чтобы использовать наследование по полной
# мы делаем не специализированный класс для логов
# а расширяем возможности FileManager чтобы он умел читать логи, это может пригодиться в любой папке
# и делаем интерфейс для его подключения в админку

class FolderAdmin(admin.ModelAdmin):
    folder = None
    model = None
    #template = 'filemanager/iframe_block.html'
    files = {}
    template = 'logtail/logtail_list.html'

    def __init__(self, model, admin_site):
        super(FolderAdmin, self).__init__(model, admin_site)
        self.fm = ImbeddedFileManager(self.folder)

    class Media:
        js = (
            'admin/js/jquery.min.js', 'admin/js/jquery.init.js',
            'logtail/js/logtail.js',
        )
        css = {
            'all': ('logtail/css/logtail.css',)
        }

    def has_add_permission(self, request):
        return False

    def log_view(self, request, logfile='', seek_to='0'):
        context = self.log_context(logfile, seek_to)
        return HttpResponse(
            self.iter_json(context),
            content_type='application/json'
        )

    def change_view(self, request, path, *args, **kwargs):
        if 'download' in request.GET:
            return self.fm.download(path, request.GET['download'])
        return self.fm.media(path)


    def log_context(self, logfile, seek_to):
        context = {}
        seek_to = int(seek_to)

        # TODO LOGTAIL_FILES используется только здесь
        # Q: зачем вообще каждый раз полностью его собирать
        # A: Что бы при добавлении нового файла через фолдерменеджер мы могли на него нажать
        try:
            log_file = self.files[logfile]
        except KeyError:
            raise Http404('No such log file')

        try:
            file_length = getsize(log_file)
        except OSError:
            raise Http404('Cannot access file')

        if seek_to > file_length:
            seek_to = file_length

        try:
            context['log'] = file(log_file, 'r')
            context['log'].seek(seek_to)
            context['starts'] = seek_to
        except IOError:
            raise Http404('Cannot access file')

        return context

    @staticmethod
    def iter_json(context):
        yield '{"starts": "%d",' \
               '"data": "' % context['starts']

        while True:
            line = context['log'].readline()
            if line:
                yield json.dumps(line).strip(u'"')
            else:
                yield '", "ends": "%d"}' % context['log'].tell()
                context['log'].close()
                return

    def changelist_view(self, request, extra_context=None):

        for directory, directories, files in os.walk(self.folder):
            for f in files:
                if f.lower().split(".")[-1] == "log":
                    filename = f.lower().split("/")[-1]
                    filepath = os.path.join(directory, f)
                    if filepath not in self.files.values():
                        self.files[filename] = os.path.join(directory, f)

        context = dict(
            title='Logtail',
            app_label=self.model._meta.app_label,
            cl=None,
            media=self.media,
            has_add_permission=self.has_add_permission(request),
            update_interval=app_settings.LOGTAIL_UPDATE_INTERVAL
        )

        context.update(self.fm.render(request, ""))

        logfiles = ((l, f) for l, f in self.files.iteritems() if isfile(f))

        context.update({"logfiles": logfiles})
        return TemplateResponse(
            request,
            self.template,
            context,
        )

    def get_urls(self):
        urls = super(FolderAdmin, self).get_urls()
        urls.insert(0,
                    url(r'^(?P<logfile>[-\w\.]+)/(?P<seek_to>\d+)/$',
                        self.admin_site.admin_view(self.log_view),
                        )
                    )
        urls.insert(1, url(r'^([-\w]*?[.]+[-\w]*/?)$', self.view()))
        return urls

    def view(self):
        def filemanager(request, path):
            return self.fm.render(request, path)
        return filemanager

    @classmethod
    def register(cls):
        admin.site.register(cls.model, cls)