import pprint
import time

from django.conf import settings
from django.core.management import BaseCommand
from django.utils.module_loading import import_string
from maintenance.utils import log
from daemons.management.commands.angel import Command as Angel


class Command(BaseCommand):
    COMMANDS = None

    def handle(self, *args, **options):
        self.COMMANDS = getattr(settings, "SCHEDULER_COMMANDS", None)
        if self.COMMANDS is None:
            raise Exception("please, set SCHEDULER_COMMANDS in settings")

        commands = {}
        for command in self.COMMANDS:
            model = import_string("%s.Command" % command)
            commands[command] = {
                "model": model,
                "next_call": model.next_call(),
                "name": command.split('.')[-1],
            }

        log("commands: %s" % pprint.pformat(commands))

        while True:
            time.sleep(60)

            for command, config in commands.iteritems():
                next_scheduled_call = config["model"].next_call()

                if config["next_call"] != next_scheduled_call:
                    log("going to run process %s" % command)
                    Angel.run_django_command("%s --force" % config['name'])
                    config["next_call"] = next_scheduled_call
                    log("next call schedulled at %s" % next_scheduled_call)
