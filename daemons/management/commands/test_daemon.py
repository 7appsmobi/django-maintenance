import time

from django.core.management import BaseCommand
# from daemons.utils import log
from maintenance.utils import log


class Command(BaseCommand):
    def handle(self, *args, **kwrags):
        log("args: %s, kwargs: %s" % (args, kwrags))
        while True:
            log('working')
            time.sleep(2)
