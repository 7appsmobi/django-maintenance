import pprint


def add_debug_panel(settings):
    if 'debug_toolbar' not in settings['INSTALLED_APPS']:
        settings['INSTALLED_APPS'] += ('debug_toolbar', )

    if 'debug_toolbar.middleware.DebugToolbarMiddleware' not in settings['MIDDLEWARE_CLASSES']:
        settings['MIDDLEWARE_CLASSES'] += ('debug_toolbar.middleware.DebugToolbarMiddleware', )

    settings['DEBUG_TOOLBAR_PATCH_SETTINGS'] = False
    settings["DEBUG_TOOLBAR_ACTIVATED"] = True

    if 'DEBUG_TOOLBAR_CONFIG' not in settings:
        settings['DEBUG_TOOLBAR_CONFIG'] = get_debug_toolbar_config(settings)


def get_debug_toolbar_config(settings):
    return {
       'JQUERY_URL': settings['STATIC_URL'] + 'maintenance/js/jquery-2.1.1.min.js',
       'SHOW_TOOLBAR_CALLBACK': 'maintenance.debug.show_toolbar_superuser'
    }


def show_toolbar_superuser(request):
    from django.conf import settings

    if not request.user.is_superuser:
        return False

    if request.is_ajax():
        return False

    return bool(settings.DEBUG)

